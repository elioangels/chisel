<aside>
  <dl>
  <dd>And all the Prophets in their age the times</dd>
  <dd>Of great Messiah shall sing. Thus, laws and rites</dd>
  <dd>Established, such delight hath God in Men</dd>
  <dd>Obedient to his will</dd>
</dl>
</aside>

A documentation website builder done **the elioWay**.

**chisel** requires very little configuration because it's so opioninated. It's purpose built for **elioWay** apps and libraries which it assumes will all follow the same _pattern_, which is described in the Quickstart. Therefore chisel won't work unless you opt to use the same documentation pattern.

**chisel** offers only two themes, both of them part of the **eliosin** framework: **rei** for the product landing page - aka the index; and **sloth** for the documentation detail pages.

# Seeing is Believing

```
cd ~/elioway/elioangels/chisel
npm|yarn link
cd ~/elioway/
chisel
```
