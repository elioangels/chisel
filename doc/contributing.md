# Contributing

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/elioangels.git
cd elioangels
git clone https://gitlab.com/elioangels/chisel.git
cd chisel
```

## TODOS

1. When it runs, we have to manually copy **eliosin/icons** into the public folder. Yuk.
2. More automation of Features.
3. Move into the **tew** CLI app.
4. Rename `raphael`?
