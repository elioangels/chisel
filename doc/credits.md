# Credits

## Artwork

- [wikimedia:Tomb_of_Rekhmire](https://upload.wikimedia.org/wikipedia/commons/f/f0/Masons_Squaring_a_Block%2C_Tomb_of_Rekhmire_MET_chr31.6.23.jpg)
- [wikimedia:Egyptian-qdwt](https://commons.wikimedia.org/wiki/File:Egyptian-qdwt_1.PNG)
- [asciiart:Linda Ball:Birdies](https://www.asciiart.eu/animals/birds-land)

## These were helpful!

- [medium:@thatisuday:creating-cli-executable](https://medium.com/@thatisuday/creating-cli-executable-global-npm-module-5ef734febe32)
- [medium:turpialdev:gitlab-pages](https://medium.com/turpialdev/how-to-host-an-html-page-in-gitlab-pages-47bbfbdeb4d1)

## Consider Using

- [fiddly](https://fiddly.netlify.app/)
- <https://oclif.io/docs/introduction>
