# Quickstart chisel

- [chisel Prerequisites](/elioangels/chisel/prerequisites.html)

## Project structure

- You should group related modules/projects/repos in a host repo folder. The host will only host the documentation. The sub repos will be properly made `.gitmodules`. We already have 4 elioWay groups called elioangels, eliosin, eliothing, elioFaithful.

```
elio_host_app_root
|   .gitmodules
└───mainapp1
└───mainapp2
└───mainapp3
└───other1
└───misc1
└───misc2
```

- Including the host, and in all the sub repositories, have at least the following:

```
elio_sub_app_root
│   apple-touch-icon.png
│   elio-appname-logo.png
│   favicon.ico
│   favicon.ico
│   package.json
|   LICENSE
|   README.md
|   splash.jpg
│   star.png
└───doc
    │   contributing.md
    │   credits.md
    │   index.md
    │   installing.md
    │   prerequisites.md
    │   quickstart.md
```

In the host app you will also want to include the following:

### `.gitlab-ci.yml`

The gitlab continuing integration script which will publish the static html files **chisel** creates to **gitlab pages**.

```
pages:
  stage: deploy
  script:
  - mkdir public/ || true
  artifacts:
    paths:
    - public
  only:
  - main
```

### `img`

Any images you referenced in the markdown files.

## Chisel configuration

NB: See [generator-art](/elioangels/generator-art) for artwork specification.

- Start with a host repo. Add a `chisel` section to its `package.json` file:

```
"tew": {
  "lifecycle": "experimental",
  "chisel": {
    "host": "hostapp",
    "core": [
      "mainapp1",
      "mainapp2",
      "mainapp3"
    ],
    "other": [
      "other1"
    ],
    "misc": [
      "misc1",
      "misc2"
    ]
  }
},
```

- Add the same to each `package.json` file of the sub repositories mentioned. You can add and exclude everything except the `host`.

## Important

In the boilerplates below, if you are creating the file for the host app, use the `hostappname` instead of the `appname`, and drop the `appname` from the path in the links.

## `index.md`

- Your `index.md` file should start from this boilerplate:

The available project statuses are as follows:

- ![alpha](/eliosin/icon/devops/alpha/favicon.ico "alpha")
- ![beta](/eliosin/icon/devops/beta/favicon.ico "beta")
- ![experimental](/eliosin/icon/devops/experimental/favicon.ico "experimental")
- ![notstarted](/eliosin/icon/devops/notstarted/favicon.ico "notstarted")
- ![release candidate](/eliosin/icon/devops/rc/favicon.ico "release candidate")
- ![release](/eliosin/icon/devops/release/favicon.ico "release")

```
# appname

<figure>
  <img src="star.png" alt="">
</figure>

> <Quote related to app> **the elioWay**

![status](/static/status.png "status")

App blurb
```

You can add more buttons.

You can add full page width, 3 feature tile promotion linking to other markdown files you can optionally create, like `doc/STUFF1.md`, `doc/STUFF2.md` and `doc/STUFF3.md`,

```
<section>
  <figure>
  <img src="STUFF1/star.png">
  <h3>STUFF1</h3>
  <p>STUFF1 blurb.</p>
  <p><a href="STUFF1/STUFF1.html" class="elioButton">STUFF1 link</a></p>
</figure>
  <figure>
  <img src="STUFF2/star.png">
  <h3>STUFF2</h3>
  <p>STUFF2 blurb.</p>
  <p><a href="STUFF2/STUFF2.html" class="elioButton">STUFF2 link</a></p>
</figure>
  <figure>
  <img src="STUFF3/star.png">
  <h3>STUFF3</h3>
  <p>STUFF3 blurb.</p>
  <p><a href="STUFF3/STUFF3.html" class="elioButton">STUFF3 link</a></p>
</figure>
</section>
```

## `quickstart.md`

- Your `quickstart.md` file should start from this boilerplate:

```
# appname Quickstart

- [Prerequisites](/elioangels/chisel/prerequisites.html)
## Nutshell Checklist

- A
- Quick
- Reminder
- And outline of the rest of this page (probably)

## Task 1

And so on...

## Task 2

And so on...

## Next

Recommend what happens next. Link to related optional markdown files (e.g `api.md`) or other resources.

- [API documentation](/elioangels/chisel/api)
```

## `installing.md`

- Your `quickstart.md` file should start from this boilerplate:

```
# Installing appname

## Step 1

And so on...
```

## `credits.md`

- Your `credits.md` file should start from this boilerplate:

```
# appname Credits

## Core! Ta very much.

Links to core frameworks you used.

## These were useful

Links to stackoverflow answers, blogs, guides, books, etc which were particularly helpful... not too many... the ones you relied on.

## Artwork

A place to link credits for public domain or creative open license artwork. This is not just to be nice, but also so that you can find it again if you lose the original!
```

## `README.md`

The `README.md` is not included in the chiselled documentation, but you'll want to change the links in the file to link to the documentation (in its `doc` folder) once it's published.

- Your `README.md` file should start from this boilerplate:

```
![](/hostappname/appname/elio-appname-logo.png)

> <Quote related to app> **the elioWay**

# appname ![status](/static/status.png "status")

appname description. Not too long. Use the doc/index.md file for a full description.

- [appname Documentation](/hostappname/appname)
- [appname Demo](/hostappname/appname/demo.html)

## Installing

Any relevant single command.

- [Installing appname](/hostappname/appname/installing.html)

## Seeing is Believing

If possible, a handful of relevant commands which would let the reader see the app working quickly.

## Nutshell

### `relevant common command1`

### `relevant common command2`

### `relevant common command3`

- [appname Quickstart](/hostappname/appname/quickstart.html)
- [appname Credits](/hostappname/appname/credits.html)
- [appname Issues](/hostappname/appname/issues.html)

![](/hostappname/appname/apple-touch-icon.png)

## License

[MIT](LICENSE)
```

## chisel

Now you are ready to **chisel**.

With all the above set up:

```
set L elioangels eliobones elioflesh eliosin eliothing
cd ~/Dev/elioway/
for R in $L
    cd $R
    chisel
    cd ..
end
chisel
gulp
for R in $L
   cd $R
   git add .
   git commit -m "docs"
   git push
   cd ..
end
git add .
git commit -m "docs"
git push
```

All the markdown documentation from the sub apps (and the host app) are compiled to html in the host apps `public` folder.

Now all you need to do is push the newly compiled documentation to gitlab:

```
git add .
git commit -m "new documentation"
git push
```

And wait... then go to: `/hostappname/appname`

For instance, this page is hosted... wait... duh... it's hosted [where you are right now!](/elioangels/chisel)

```
rm -rf public
chisel
git push
git commit -m "artwork folder + repo test"
git add .
twig commit master "artwork+docs"
twig push master
git add .
git commit -m "docs"
git push
```
