# Installing chisel

- [chisel Prerequisites](/elioangels/chisel/prerequisites.html)
- [Installing chisel](/elioangels/chisel/installing.html)

## Install globally

```shell
npm install -g @elioway/chisel --save
yarn add global @elioway/chisel
```

## CI GitLab

- <https://docs.gitlab.com/runner/install/linux-repository.html>
- <https://docs.gitlab.com/runner/register/index.html>

```shell
sudo apt update
sudo apt upgrade -y
sudo apt autoremove
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner
sudo gitlab-runner register

# WIZARD!
Enter the GitLab instance URL (for example, https://gitlab.com/):
>> https://gitlab.com/
Enter the registration token:
>> uTT3R5h1tB100DdyMess
Enter a description for the runner:
>> [your machine name is fine]:
Enter tags for the runner (comma-separated):
>> elioway
Enter optional maintenance note for the runner:
>>
Registering runner... succeeded                     runner=GR1348941GhUrTNGD
Enter an executor: docker+machine, docker-ssh+machine, docker, parallels, ssh, virtualbox, instance, kubernetes, custom, docker-ssh, shell:
>> docker
Enter the default Docker image (for example, ruby:2.7):
>> ruby:2.7
```
