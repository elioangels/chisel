require("../../tablet/lib/String.prototype.md.js")

global.should = require("chai").should()

var TESTMARKDOWN = `# TITLE



Introductory "mess-sage" from the pit (or hell if you like).

Hope you are okay.

## TITLE2`

describe("lib/String.prototype.md ", function () {
  it("String.prototype.splitAndDice", done => {
    let xthis = "/home/tim/repo//elioApps/elioFaithful/tablets1/tablets2"
    xthis
      .splitAndDice("/", -4, null, ">")
      .should.equal("elioApps>elioFaithful>tablets1>tablets2")
    xthis
      .splitAndDice("/", -4, 0, ">")
      .should.equal("elioApps>elioFaithful>tablets1>tablets2")
    xthis
      .splitAndDice("/", -4, -1, ">")
      .should.equal("elioApps>elioFaithful>tablets1")
    xthis.splitAndDice("/", -4, -2, ">").should.equal("elioApps>elioFaithful")
    xthis.splitAndDice("/", -4, -3, ">").should.equal("elioApps")
    xthis.splitAndDice("/", -4, -4, ">").should.equal("")
    xthis.splitAndDice("/", -4, -5, ">").should.equal("")
    done()
  })
})

describe("lib/String.mdHeadingLevel.md ", function () {
  it("returns the correct mdHeadingLevel", done => {
    let xthis = "/home/tim/repo//elioApps/elioFaithful/tablets1/tablets2"
    "# TITLE   ".mdHeadingLevel().should.equal(1)
    "## TITLE   ".mdHeadingLevel().should.equal(2)
    "### TITLE   ".mdHeadingLevel().should.equal(3)
    "#### TITLE   ".mdHeadingLevel().should.equal(4)
    "##### TITLE   ".mdHeadingLevel().should.equal(5)
    "###### TITLE   ".mdHeadingLevel().should.equal(6)
    "####### TITLE   ".mdHeadingLevel().should.equal(7)
    done()
  })
})

describe("lib/String.mdHeadings.md ", function () {
  it("returns the correct mdHeadings", done => {
    TESTMARKDOWN.mdHeadings().should.deep.equal([
      { title: "TITLE", id: "title", level: 1 },
      { title: "TITLE2", id: "title2", level: 2 },
    ])
    TESTMARKDOWN.mdHeadings(1, 6).should.deep.equal([
      { title: "TITLE", id: "title", level: 1 },
      { title: "TITLE2", id: "title2", level: 2 },
    ])
    TESTMARKDOWN.mdHeadings(1, 1).should.deep.equal([
      { title: "TITLE", id: "title", level: 1 },
    ])
    TESTMARKDOWN.mdHeadings(2, 2).should.deep.equal([
      { title: "TITLE2", id: "title2", level: 2 },
    ])
    TESTMARKDOWN.mdHeadings(3, 6).should.deep.equal([])
    done()
  })
})

describe("lib/String.mdDesc.md ", function () {
  it("returns the correct mdDesc", done => {
    TESTMARKDOWN.mdDesc().should.equal(
      "Introductory mess-sage from the pit (or hell if you like)."
    )
    done()
  })
})
