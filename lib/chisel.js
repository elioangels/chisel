"use strict"
require("./String.prototype.md.js")
let fs = require("fs")
let ejs = require("ejs")
let sh = require("shelljs")
let { marked } = require("marked")

var silentState = sh.config.silent // save old silent state
sh.config.silent = true

let chiselRoot = __dirname.splitAndDice("/", 0, -1, "/")
const TEMPLATES = {
  docs: fs.readFileSync(`${chiselRoot}/templates/docs.ejs`, "utf-8"),
  home: fs.readFileSync(`${chiselRoot}/templates/home.ejs`, "utf-8"),
}

let chiselAppDocs = function (appDir, subjectOf, identifier) {
  let packageData = fs.readFileSync(`${appDir}/package.json`)
  let packageJson = JSON.parse(packageData)

  let buildFolder = `${buildRoot}/public`
  if (identifier.toLowerCase() !== subjectOf.toLowerCase()) {
    buildFolder = `${buildRoot}/public/${identifier}`
  }
  try {
    sh.mkdir("-p", `${buildFolder}`)
    sh.mkdir("-p", `${buildFolder}/artwork`)
    sh.cp(
      "-f",
      `${appDir}/apple-touch-icon.png`,
      `${appDir}/elio-${identifier.replace("elio", "")}-logo.png`,
      `${appDir}/star.png`,
      `${appDir}/postcard.jpg`,
      `${buildFolder}/`
    )
    sh.cp("-f", `${appDir}/index.html`, `${buildFolder}/demo.html`)
    sh.cp("-f", `${appDir}/favicon.*`, `${buildFolder}`)
    sh.cp("-Rf", `${appDir}/artwork/*`, `${buildFolder}/artwork`)
    if (fs.existsSync(`${appDir}/dist/`)) {
      sh.mkdir("-p", `${buildFolder}/dist`)
      sh.cp("-Rf", `${appDir}/dist/*`, `${buildFolder}/dist`)
    }
  } catch (err) {
    console.error(`${err}`)
  }

  // Process all the markdown files in the doc folder.
  fs.readdir(`${appDir}/doc`, function (err, files) {
    files.forEach(function (mdfile, index) {
      let msFileName = mdfile.split(".")[0]
      let isMdFile = mdfile.split(".").slice(-1)[0] === "md"
      if (isMdFile) {
        fs.readFile(`${appDir}/doc/${mdfile}`, "utf8", function (err, data) {
          if (err) {
            console.log(`Error reading ${appDir}/doc/${mdfile}`)
          } else {
            let packageName = packageJson.name.split("/").pop()
            let template = TEMPLATES.docs
            if (msFileName === "index") {
              template = TEMPLATES.home
            }
            let mainHeadings = data.mdHeadings(1)
            let mdSubHeadings = data.mdHeadings(2, 2)
            if (!mainHeadings.length) {
              mainHeadings = [{ title: packageName }]
            }
            let merge = {
              mdHTML: marked(data),
              mdPageTitle: mainHeadings[0],
              mdHeadings: mdSubHeadings,
              hasHeadings: mdSubHeadings.length,
              packageDesc: packageJson.description,
              chisel: packageJson.tew.chisel,
              subjectOf: packageJson.tew.chisel.Home,
              identifier: packageJson.name.split("/").pop(),
              repository: packageJson.repository.url,
              lifecycle: packageJson.tew.lifecycle,
            }
            fs.writeFile(
              `${buildFolder}/${msFileName}.html`,
              ejs.render(template, merge),
              function (err) {
                if (err) {
                  console.log(`Error writing ${msFileName}.html: ${err}`)
                }
              }
            )
          }
        })
      }
    })
  })
}

/**
 * Process group folder
 */
let chiselGroup = (groupDir, subjectOf) => {
  let packageFile = `${groupDir}/package.json`
  if (fs.existsSync(packageFile)) {
    let packageData = fs.readFileSync(packageFile)
    let packageJson = JSON.parse(packageData)
    // packageJson has a chisel config
    if (packageJson.tew.chisel) {
      // chisel the group
      chiselAppDocs(groupDir, subjectOf, packageJson.name.split("/").pop())
      console.log(`                 (o> ${subjectOf}`)
      // chisel all the apps mentioned
      Object.keys(packageJson.tew.chisel).forEach(function (key) {
        if (key !== "Home") {
          packageJson.tew.chisel[key].forEach(function (identifier) {
            let appDir = `${groupDir}/${identifier}`
            chiselAppDocs(appDir, subjectOf, identifier)
            console.log(`                 (o> ${subjectOf}/${identifier}`)
          })
        }
      })
    }
  }
}

let cwd = sh.pwd()
let buildRoot
if (!buildRoot || buildRoot === "./") {
  buildRoot = cwd["stdout"]
} else {
  buildRoot = "/" + process.argv[2].split("/").filter(Boolean).join("/")
}

let subjectOf = buildRoot.split("/").slice(-1)

exports.chisel = function () {
  console.log(
    `
    \\       ┌─────────────────────────┐
    (o>  ───│ Welcome to (elio)chisel │
    (()     └─────────────────────────┘
    ||


    Chiseling... ${buildRoot}`
  )

  chiselGroup(buildRoot, subjectOf[0])
}
