"use strict"

Array.prototype.dice = function (start, end) {
  return this.slice(start, end ? end : this.length)
}

String.prototype.splitAndDice = function (lim, start, end, rtnlim) {
  if (!rtnlim) rtnlim = lim
  return this.split(lim).dice(start, end).join(rtnlim)
}

String.prototype.mdHeadingLevel = function () {
  let level = 1
  while (this.slice(level - 1, level) === "#") level++
  return level - 1
}

String.prototype.mdHeadings = function (start, end) {
  if (!start) start = 1
  if (!end) end = 6
  return this.split("\n")
    .filter(line => line.slice(0, 1) === "#")
    .filter(line => {
      let level = line.mdHeadingLevel()
      return level >= start && level <= end
    })
    .map(line => {
      let mdHD = line.mdHeadingLevel()
      return {
        title: line.slice(mdHD + 1),
        id: line.slice(mdHD + 1).slugify(),
        level: mdHD,
      }
    })
}

String.prototype.mdDesc = function () {
  let rtn = this.split("\n")
    .filter(line => line.slice(0, 1) != "#" && line.slice(0, 1) != "#")
    .filter(line => line != null && line.trim() != "")
    .slice(0, 1)
    .toString()
    .match(/[\ A-Za-z0-9\-\.\(\)]/g)

  if (rtn) return rtn.join("")
}

String.prototype.slugify = function () {
  let rtn = this.toLowerCase()
    .trim()
    .replace(
      /[\u2000-\u206F\u2E00-\u2E7F\\'!"#$%&()*+,./:;<=>?@[\]^`{|}~]/g,
      ""
    )
    .replace(/\s/g, "-")

  if (rtn) return rtn
}
