![](https://elioway.gitlab.io/elioangels/chisel/elio-chisel-logo.png)

> Better than nothing **Dick Brandon**

# chisel ![experimental](https://elioway.gitlab.io/eliosin/icon/devops/experimental/favicon.ico "experimental")

Documentation creation, **the elioWay**.

- [chisel Documentation](https://elioway.gitlab.io/chisel)

## Installing

```shell
npm install -g @elioway/chisel --save
yarn add global @elioway/chisel
```

## Seeing is Believing

```
cd ~/elioway/elioangels/chisel
npm|yarn link
cd ~/elioway/
chisel
```

- [chisel Quickstart](https://elioway.gitlab.io/elioangels/chisel/quickstart.html)
- [chisel Credits](https://elioway.gitlab.io/elioangels/chisel/credits.html)

![](https://elioway.gitlab.io/elioangels/chisel/apple-touch-icon.png)

## License

[MIT](LICENSE.txt) [Tim Bushell](mailto:theElioWay@gmail.com)
